function blockInsecureRequests(requestDetails) {
  if (requestDetails.url.match(/^http:/)) {
    const new_url = requestDetails.url.replace(/^http:/,"https:");
    console.log(`Redirecting ${requestDetails.url} to ${new_url}`);
    return { redirectUrl: new_url };
  } else {
    return;
  }
}

function startEnforcing() {
  browser.webRequest.onBeforeRequest.addListener(
    blockInsecureRequests,
    {urls: ["<all_urls>"]},
    ["blocking"]
  );
  browser.browserAction.setIcon({path: "icons/shield-on.svg"});
}

function stopEnforcing() {
  browser.webRequest.onBeforeRequest.removeListener(blockInsecureRequests);
  browser.browserAction.setIcon({path: "icons/shield-off.svg"});
}

function toggleHttpsOnly() {
  if (browser.webRequest.onBeforeRequest.hasListener(blockInsecureRequests)) {
    stopEnforcing();
  } else {
    startEnforcing();
  }
}

browser.browserAction.onClicked.addListener(toggleHttpsOnly);

startEnforcing();
